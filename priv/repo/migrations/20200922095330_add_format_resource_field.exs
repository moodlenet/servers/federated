defmodule MoodleNet.Repo.Migrations.AddFormatResourceFields do
  use Ecto.Migration

  def change do
      alter table("mn_resource") do
      add :format, :string
    end
  end
end
